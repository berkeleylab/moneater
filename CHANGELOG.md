v0.1.0
 - Add `fileeater` program for monitoring (multiple) log files.
 - Add `monexec` module with common functions inside moneater programs.
 - InfluxDB connection settings can be stored inside a YAML file.
v0.0.1
  - Initial release.
