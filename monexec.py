import argparse
import datetime
import importlib
import time
import yaml

from influxdb import InfluxDBClient

"""
Library that can run Moneater.
"""

class MoneaterFactory:
    """
    Create Moneater objects based on user-supplied settings
    """
    def __init__(self, description):
        """
        Initialize factory `ArgumentParser` with common settings. It
        does not parse them yet.
        """
        self.parser = argparse.ArgumentParser(description=description)

        # Common arguments (DB settings)
        self.parser.add_argument('--host'     ,default='localhost',
                                 help='Host with InfluxDB')
        self.parser.add_argument('--port','-p',type=int,
                                 help='Port with InfluxDB')
        self.parser.add_argument('--user','-u',
                                 help='InfluxDB user')
        self.parser.add_argument('--password' ,
                                 help='InfluxDB password')
        self.parser.add_argument('--database' ,'-d',
                                 help='Target database')
        self.parser.add_argument('--tag'      ,action='append',
                                 help='Tag for every measurement in format TAG=VALUE')
        self.parser.add_argument('--config'   ,'-c',type=str,
                                 help='Moneater database configuration YAML file.')

        # Final parsed results will be stored here
        self.args = None

        self.db = None

    def parse(self):
        """
        Parse the arguments via `parser`. The results will be stored under
        `self.args`.
        """
        self.args = self.parser.parse_args()

    def database(self):
        """
        Create and initialize a `Database` object based on arguments. The object
        is also cached under `self.db`.
        """
        if self.args is None:
            raise Exception("Arguments not parsed.")

        if self.db is None:
            self.db=Database()
            if self.args.config is not None:
                self.db.load_config(self.args.config)

            if self.args.host     is not None:
                self.db.host    =self.args.host
            if self.args.port     is not None:
                self.db.port    =self.args.port
            if self.args.user     is not None:
                self.db.user    =self.args.user
            if self.args.password is not None:
                self.db.password=self.args.password
            if self.args.database is not None:
                self.db.database=self.args.database

            if self.args.tag:
                tags = {opt.split('=')[0]:opt.split('=')[1] for opt in self.args.tag}
                self.db.tags.update(tags)

            self.db.connect()
        return self.db
        
def update_tags(points, tags):
    """
    Update tags of points by appending contents of the
    `tags` dictionary.
    """
    for point in points:
        mytags=tags.copy()
        if 'tags' in point:
            mytags.update(point['tags'])
        point.update({
            'tags'       :mytags
        })

def get_eater(eaterstr):
    """
    Return eater object corresponding to class located at `eaterstr`.
    """
    eater_parts =eaterstr.split('.')
    eater_module='.'.join(eater_parts[:-1])
    eater_class =eater_parts[-1]
    eater_module=importlib.import_module(eater_module)

    eater=getattr(eater_module, eater_class)()

    return eater

class Database:
    """
    Database (InfluxDB) connection.

    The `Database` object consists of the following properties:
    - `host`: Hostname or IP of the database server.
    - `port`: Port on with the database is running.
    - `user`: User for authentication (optional, `None` to ignore).
    - `password`: Passsword for authentication (optional, `None` to ignore).
    - `database`: Name of the database to send data.

    The properties can either be set directly or loaded from a YAML file. See
    `Database.load_config` on how to do the latter.

    Optional tags can be appended to each upload.
    """
    def __init__(self):
        # Connection information
        self.host = 'localhost'
        self.port = 8086
        self.user = None
        self.password = None
        self.database = None

        # Extra upload information
        self.tags = {}

        # Connection
        self.client = None
    
    def load_config(self, path):
        """
        Load database settings from a YAML file.

        The file should contain the following block:

        ```yaml
        db:
          host: localhost
          port: 8086
          user: root
          password: root
          database: mydb
        tags:
          name: value
        ```
        """
        with open(path) as fh:
            conf=yaml.safe_load(fh)
            dbconf=conf['db']
            self.host     = dbconf.get('host'    ,self.host)
            self.port     = dbconf.get('port'    ,self.port)
            self.user     = dbconf.get('user'    ,self.user)
            self.password = dbconf.get('password',self.password)
            self.database = dbconf.get('database',self.database)

            tagconf=conf.get('tags', {})
            self.tags=tagconf

    def connect(self):
        """
        Connect to database using object settings. The settings cannot be
        set after this is called.
        """
        if self.client is not None:
            raise Exception('Connection already exists!')

        self.client = InfluxDBClient(self.host, self.port, self.user, self.password, self.database)

    def write_points(self, points):
        """
        Upload new points to the database. The format is a list as specified
        by the influxdb package.
        """
        if self.client is None:
            raise Exception('Connection does not exist!')

        # Add tags
        update_tags(points, self.tags)

        # Upload
        self.client.write_points(points)

class Moneater:
    def __init__(self,fh,eater,table,database,tags={}):
        """
        Monitor the contents of stream `fh` using class `eater` and
        upload the results to `database`/`table` with optional `tags`.
        """
        self.fh=fh
        self.eater=eater
        self.table=table
        self.database=database
        self.tags=tags

    def run_line(self):
        """
        Check for new data inside stream being watched
        """
        line=self.fh.readline()
        if not line:
            time.sleep(0.1) # Sleep briefly
            return
        line=line.strip()
        if line=='': return # Nothing to parse

        # parse
        points=self.eater.parse_line(line)
        if points is None: return # No new data

        # Turn into a list
        if type(points)!=list:
            points=[points]

        # Check if they are only points or the full format
        now=datetime.datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S.%fZ')
        points=[{'time':now, 'fields': point } if 'fields' not in point else point for point in points]
 
        # Add tags and table
        update_tags(points, self.tags)
        for point in points:
            point.update({
                'measurement':self.table,
            })

        # Upload
        self.database.write_points(points)

class Moneaters:
    """
    Multiple `Moneater` objects that should be run in "parallel".

    Provides the same interface as a single Moneater to allow for
    seamless usage.
    """
    def __init__(self, moneaters=[]):
        self.moneaters=moneaters

    def run_line(self):
        for moneater in self.moneaters:
            moneater.run_line()
